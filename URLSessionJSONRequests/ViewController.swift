//
//  ViewController.swift
//  URLSessionJSONRequests
//
//  Created by amr on 3/6/18.
//  Copyright © 2018 amr. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
     
    }

    @IBAction func getClicked(_ sender: Any) {
        guard let url = URL(string: "https://jsonplaceholder.typicode.com/POSTS") else { return }
        
        let session = URLSession.shared
        session.dataTask(with: url) { (data, response, error) in
            if let response = response {
                print(response)
            }
            if let data = data {
                print(data)
                do{
                    let json = try JSONSerialization.jsonObject(with: data, options: [])
                    print(json)
                } catch {
                    print(error)
                    
                }
            }
        }.resume()
    }
    
    @IBAction func postClicked(_ sender: Any) {
        
        let parameters = ["username": "@kokowawa", "tweet": "HELLO WORLD"]
        
        guard let url = URL(string: "https://jsonplaceholder.typicode.com/POSTS") else { return }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else { return }
        request.httpBody = httpBody
        
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if let response = response {
                print(response)
            }
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: [])
                    print(json)
                } catch {
                    print(error.localizedDescription)
            }
        }
    }.resume()

    }
}

